# <img src="img/document-flat-smalllikeart.svg"  width="32" height="32"> Pré-projeto para Mestrado

Meu pré-projeto para inscrição na seleção de mestrado da Pós-Graduação do CIn-UFPE do edital 2021.1.

## Tema escolhido

- **Linha de pesquisa**: Engenharia de Software e Linguagens de Programação
- **Título**: Teste de Regressão Efetivo, Eficiente e Escalável
- **Proponente**: Breno Miranda

Teste de software consome grande parte dos esforços investidos durante o processo de desenvolvimento e manutenção de software e teste de regressão (TR) é, de longe, a mais cara entre as atividades de teste.
O objetivo do teste de regressão é garantir que modificações introduzidas no software sendo testado (SUT) não tenham quebrado funcionalidades existentes.
Idealmente, todos os testes criados para versões anteriores deveriam ser reexecutados para garantir que o software continua funcionando como esperado.
Entretanto, reexecutar todos os testes após cada mudança no SUT torna-se rapidamente inviável: à medida em que o SUT cresce em tamanho e complexidade o mesmo acontece com a suíte de testes devido à adição de novos casos de teste.
Por outro lado, deixar de executar casos de teste cruciais pode trazer sérias consequências.
O principal desafio associado à TR é, portanto, o de reduzir os esforços de reteste sem impactar a sua efetividade. Nesse contexto, inúmeras técnicas de TR foram propostas ao longo dos anos com o objetivo de otimizar a relação custo-benefício dos testes de regressão [1, 2].
Em particular, 3 dimensões se destacam na literatura: Redução (ou minimização),
Seleção e Priorização de casos de teste.
Infelizmente, muitas das técnicas propostas não podem ser aplicadas em contextos reais de teste porque a abordagem proposta funciona, em tempo aceitável, apenas para exemplos muito pequenos e não escalam para problemas industriais reais.
Em um trabalho recente, Memon e coautores [3] reportaram que 150 milhões de casos de teste são executados
diariamente na Google.
Para problemas de larga escala do mundo real, precisamos de soluções que possam ser aplicadas de forma transparente a grandes suítes de testes e que possam se adaptar ao contexto.
Uma alternativa possível é a aplicação de técnicas comumente utilizadas no domínio de big data para o processamento de conjuntos de dados massivos [4].
O objetivo geral deste projeto é o de propor, validar e automatizar soluções efetivas, eficientes e escaláveis para o problema de teste de regressão.

## Créditos de uso de imagem

<div>Ícone do repositório feito por <a href="" title="smalllikeart">smalllikeart</a> do <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>
